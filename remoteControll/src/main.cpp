#include <Arduino.h>
/*void setup() {
Serial.begin(115200);
}

void loop() {
Serial.println("Hello from DFRobot ESP-WROOM-32");
delay(1000);
}
*/

#include "WiFi.h"
#include "ESPAsyncWebServer.h"

//#include <AsyncTCP.h>



#include <Adafruit_NeoPixel.h>
#include "simpleNeoPixels.h"
char* ssid = "";  //replace
char* password =  ""; //replace

AsyncWebServer server(80);

int ledPin = 2;//BUILTIN_LED;//23;
int redPin=12;
int greenPin=13;
int bluePin=14;
const char* PARAM_MESSAGE = "message";




void setup(){
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);

  setupSimpleNeoPixels(16,8);
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");

  }

  Serial.println(WiFi.localIP());
  delay(10);
  Serial.println("LED-PIN: "+ledPin);




  server.on("/hello", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "Hello World");
    Serial.println("hello");

  });
  server.on("/pixels", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "Hello World");
    writeSimpleNeoPixel(1,0,0,100);
    Serial.println("hello");

  });
  server.on("/led/off", HTTP_GET   , [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "off");
    digitalWrite(ledPin, LOW);
    writeSimpleNeoPixel(0, LOW);
    Serial.println("OFF");
  });
  server.on("/led/on", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain","on");
    digitalWrite(ledPin, HIGH);
    writeSimpleNeoPixel(0, HIGH);
    Serial.println("ON");
  });

  server.on("/led/toggle", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain","toggled");
    digitalWrite(ledPin, !digitalRead(ledPin));
    Serial.println(digitalRead(ledPin));
  });

  server.on("/led", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", String(digitalRead(ledPin)));
    Serial.println(digitalRead(ledPin));
  });

  server.on("/rgb/red", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain","redLED toggled");
    digitalWrite(redPin, !digitalRead(redPin));
    Serial.println(digitalRead(redPin));
  });
  server.on("/rgb/green", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain","greenLED toggled");
    digitalWrite(greenPin, !digitalRead(greenPin));
    Serial.println(digitalRead(greenPin));
  });
  server.on("/rgb/blue", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain","blueLED toggled");
    digitalWrite(bluePin, !digitalRead(bluePin));
    Serial.println(digitalRead(bluePin));
  });

  // Send a POST request to <IP>/post with a form field message set to <message>
  server.on("/post", HTTP_POST, [](AsyncWebServerRequest *request){
      String message;
      if (request->hasParam(PARAM_MESSAGE, true)) {
          message = request->getParam(PARAM_MESSAGE, true)->value();
      }else if(request->hasParam("red", true)) {
          message="red:";
          message += request->getParam("red", true)->value();
      }else {
          message = "No message sent";
      }

      request->send(200, "text/plain", "Hello, POST: " + message);
      Serial.println("msg: "+message);
      //Serial.println();
  });

  server.begin();
}

void loop(){}
