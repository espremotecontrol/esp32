/*
  SimpleNeoPixels.h - Library for making neopixels more approachable.
  Created by Evelyn Masso, April 9, 2017.
*/

#pragma once

#include "Arduino.h"
#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel simpleNeoPixels;

void setupSimpleNeoPixels(int pin, int num) {
  simpleNeoPixels = Adafruit_NeoPixel(num, pin, NEO_GRB + NEO_KHZ800);
  simpleNeoPixels.begin();
}

// set the nth neopixel to a particular brightness of white
// meant to be used with val as HIGH or LOW
void writeSimpleNeoPixel(int num, int val) {
  simpleNeoPixels.setPixelColor(num, simpleNeoPixels.Color(val*255,val*255,val*255));
  simpleNeoPixels.show();
}

// set the nth neopixel to a particular rgb color
void writeSimpleNeoPixel(int num, int r, int g, int b) {
  simpleNeoPixels.setPixelColor(num, simpleNeoPixels.Color(r,g,b));
  simpleNeoPixels.show();
}
